#!/usr/bin/env python

class Input:

    def __init__(self, fromAddress, toAddress, amount):
        self.fromAddress = fromAddress
        self.toAddress = toAddress
        self.amount = amount

    def __str__(self):
        return str(self.fromAddress) + str(self.toAddress) + str(self.amount)
