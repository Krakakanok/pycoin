#!/usr/bin/env python

import uuid, random
from blockchain import Blockchain
from block import Block
from transaction import Transaction
from input import Input

# Create blockchain
bitcoin = Blockchain()
bitcoin.miningBlock(str(uuid.uuid1()))

numBlocks = random.randint(5,10)

i = 0
while i < numBlocks:
    numTransactions = random.randint(5,10)

    j = 0
    while j < numTransactions:
        t = Transaction()
        numInputs = random.randint(5,10)
        numOutputs = random.randint(5,10)
        
        k = 0
        amountInputs = 0
        while k < numInputs:
            amount = random.randint(1,10000)
            newInput = Input(str(uuid.uuid1()), str(uuid.uuid1()), amount)
            t.addInput(newInput)

            amountInputs += amount
            k+=1

        k = 0
        while k < numOutputs:
            amount = random.randint(1, 7500)
            newOutput = Input(str(uuid.uuid1()), str(uuid.uuid1()), amount)
            t.addOutput(newOutput)

            k+=1

        bitcoin.addTransaction(t)
        j+=1

    bitcoin.miningBlock(str(uuid.uuid1()))
    i+=1

nb = 0
for b in bitcoin.chain:
    print "Block number {}".format(nb)
    nb+=1

    if len(b.transactions) > 0:
        for t in b.transactions:
            print t.printTransaction()
