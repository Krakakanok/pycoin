#!/usr/bin/env python

import hashlib

from transaction import Transaction
from merkleTree import MerkleTree

class Block:

    def __init__(self, timestamp, transactions, previousHash = ''):
        self.previousHash = previousHash
        self.timestamp = timestamp
        self.transactions = transactions
        self.nonce = 0
        self.hashMerkleTreeRoot = ''
        self.hash = self.calculateHash()

    def mine(self, difficulty):

        # Generate Merkle Tree
        mkt = MerkleTree(self.transactions)
        self.hashMerkleTreeRoot = mkt.createHashMerkleRoot()

        proof = ''.join(str(e) for e in ["0"] * difficulty)

        while self.hash[0:difficulty] != proof:
            self.nonce+=1
            self.hash = self.calculateHash()

    def calculateFees(self):
        amount = 0
        for t in self.transactions:
            amount += t.calculateFees()

        return amount

    def calculateHash(self):
        blockStr = self.__str__()

        return hashlib.sha256(blockStr.encode("ascii")).hexdigest()

    def __str__(self):
        return self.previousHash + str(self.timestamp) + self.hashMerkleTreeRoot + ''.join(str(t) for t in self.transactions) + str(self.nonce)
