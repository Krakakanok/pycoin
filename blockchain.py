#!/usr/bin/env python

import datetime

from block import Block
from transaction import Transaction
from input import Input

class Blockchain:
    
    def __init__(self):
        self.chain = []
        self.__difficulty = 2
        self.__pendingTransactions = []
        self.__reward = 50

    def addTransaction(self, transaction):
        self.__pendingTransactions.append(transaction)

    def miningBlock(self, miningRewardAddress):
        previousHash = ''
        pendingTransactions = []

        if len(self.chain) != 0:
            previousHash = self.__getPreviousBlock().hash
            pendingTransactions = self.__pendingTransactions

        newBlock = Block(datetime.datetime.utcnow(), pendingTransactions, previousHash)

        newBlock.mine(self.__difficulty) 
        self.__addBlock(newBlock)

        # Clean Transactions
        self.__pendingTransactions = []

        # Add a reward
        t = Transaction()
        i = Input('', miningRewardAddress, self.__calculateReward())
        t.addOutput(i)

        self.addTransaction(t) 

    def __calculateReward(self):
        changeBlocks = 210000
        multex = len(self.chain) / changeBlocks

        if multex == 0:
            if len(self.chain) > 0:
                fees = self.__getPreviousBlock().calculateFees()
                return self.__reward + fees
            return self.__reward

        rewardAcrossTime = self.__reward / float(pow(2, multex))
        fees = self.__getPreviousBlock().calculateFees()

        return rewardAcrossTime + fees

    def __getPreviousBlock(self):
        return self.chain[len(self.chain)-1]

    def __addBlock(self, block):
        self.chain.append(block)
