#!/usr/bin/env python

import hashlib, time

class MerkleTree:

    def __init__(self, leafNodes):
        self.__leafNodes = leafNodes

    def createHashMerkleRoot(self):
        
        leafs = []
        if len(self.__leafNodes) == 0:
            return ''

        # Create leaves
        for l in self.__leafNodes:
            leafs.append(hashlib.sha256(str(l).encode('ascii')).hexdigest())

        while len(leafs) != 1:
            lon =len(leafs)%2 
            for n in range(0, len(leafs)):
                if n+1 == len(leafs) and lon != 0:
                    break
                elif n != len(leafs):
                    newNode = self.__createNode(leafs[n], leafs[n+1])
                    leafs[n] = newNode
                    del leafs[n+1]
                else:
                    break

        return leafs[0]

    def __createNode(self, node1, node2):
        nodeStr = str(node1) + str(node2)
        return hashlib.sha256(nodeStr.encode('ascii')).hexdigest()
