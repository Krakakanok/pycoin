#!/usr/bin/env python

import hashlib
import datetime
import random
import uuid
from input import Input

class Transaction:

    def __init__(self):
        self.inputs = []
        self.outputs = []

    def addInput(self, newInput):
        self.inputs.append(newInput)

    def addOutput(self, newOutput):
        self.outputs.append(newOutput)

    def calculateFees(self):
        amount = 0
        for i in self.inputs:
            amount += i.amount

        for o in self.outputs:
             amount -= o.amount

        return amount

    def printTransaction(self):
        print "Inputs:"
        for i in self.inputs:
            print 'From: {} - To: {} - Amount: {}'.format(i.fromAddress, i.toAddress, i.amount)

        print "Outputs:"
        for o in self.outputs:
            print 'From: {} - To: {} - Amount: {}'.format(o.fromAddress, o.toAddress, o.amount)


    def __str__(self):
        return "Inputs" + ''.join(str(i) for i in self.inputs) + "Inputs" + ''.join(str(o) for o in self.outputs) 
